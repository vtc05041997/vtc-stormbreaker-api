package vn.vtc.stormbreaker.entity;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBAttribute;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBDocument;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@DynamoDBDocument
public class OrderDetail {
    @DynamoDBAttribute(attributeName = "product_id")
    private String productId;

    @DynamoDBAttribute(attributeName = "amount")
    private Long amount;

    @DynamoDBAttribute(attributeName = "price")
    private Long price;

    @DynamoDBAttribute(attributeName = "quantity")
    private Integer quantity;
}