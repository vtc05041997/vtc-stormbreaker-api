package vn.vtc.stormbreaker.entity;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBAttribute;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBDocument;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@DynamoDBDocument
public class Address {

    @DynamoDBAttribute(attributeName="line1")
    private String line1;

    @DynamoDBAttribute(attributeName="city")
    private  String city;

    @DynamoDBAttribute(attributeName="country")
    private  String country;
}