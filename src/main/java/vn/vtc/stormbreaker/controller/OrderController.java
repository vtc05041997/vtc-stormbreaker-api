package vn.vtc.stormbreaker.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import vn.vtc.stormbreaker.entity.Order;
import vn.vtc.stormbreaker.repository.OrderRepository;

@RestController
public class OrderController {

    @Autowired
    private OrderRepository orderRepository;

    @PostMapping("/add/order")
    public Order saveOrder(@RequestBody Order order){
        return orderRepository.saveOrder(order);
    }

    @GetMapping("/get/order/{id}")
    public Order getOrderById(@PathVariable("id") String orderId){
        return orderRepository.getOrderById(orderId);
    }

    @DeleteMapping("/delete/order/{id}")
    public String deleteOrderById(@PathVariable("id") String orderId){
        return orderRepository.deleteOrderById(orderId);
    }

    @PutMapping("/update/order/{id}")
    public String updateOrder(@PathVariable("id") String orderId, @RequestBody Order order){
        return orderRepository.updateOrder(orderId, order);
    }

    @GetMapping("/get/order/status/{id}")
    public String getOrderStatus(@PathVariable("id") String orderId){
        return orderRepository.getOrderStatus(orderId);
    }
}
