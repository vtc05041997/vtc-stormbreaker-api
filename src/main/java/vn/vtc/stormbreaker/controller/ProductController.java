package vn.vtc.stormbreaker.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import vn.vtc.stormbreaker.entity.Product;
import vn.vtc.stormbreaker.repository.ProductRepository;

@RestController
public class ProductController {

    @Autowired
    private ProductRepository productRepository;

    @PostMapping("/add/product")
    public Product saveProduct(@RequestBody Product product){
        return productRepository.saveProduct(product);
    }

    @GetMapping("/get/product/{id}")
    public Product getProductById(@PathVariable("id") String productId){
        return productRepository.getProductById(productId);
    }

    @DeleteMapping("/delete/product/{id}")
    public String deleteCustomerById(@PathVariable("id") String productId){
        return productRepository.deleteProductById(productId);
    }

    @PutMapping("/update/product/{id}")
    public String updateProduct(@PathVariable("id") String productId, @RequestBody Product product){
        return productRepository.updateProduct(productId, product);
    }

    @GetMapping("/healcheck")
    public Integer healCheck(){
        return 200;
    }

}
