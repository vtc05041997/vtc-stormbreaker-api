package vn.vtc.stormbreaker;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class VtcStormbreakerApiApplication {

	public static void main(String[] args) {
		System.out.println("From VTC with Love");
		System.out.println("This is a WEB Project");
		SpringApplication.run(VtcStormbreakerApiApplication.class, args);
	}
}
