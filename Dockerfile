FROM eclipse-temurin:17-jdk-jammy AS build

COPY mvnw pom.xml ./
COPY .mvn/ .mvn
RUN ./mvnw dependency:resolve

COPY src ./src
RUN ./mvnw package

FROM eclipse-temurin:17-jdk-jammy
WORKDIR demo
COPY --from=build target/*.jar vtc-stormbreaker-api.jar
EXPOSE 8080

ENTRYPOINT ["java", "-jar", "vtc-stormbreaker-api.jar"]

#CMD java -classpath vn/vtc/stormbreaker VtcStormbreakerApiApplication
#CMD ["java", "vn.vtc.stormbreaker.VtcStormbreakerApiApplication"]
# syntax=docker/dockerfile:1

#FROM eclipse-temurin:17-jdk-jammy
#
#WORKDIR /app
#
#COPY .mvn/ .mvn
#COPY mvnw pom.xml ./
#RUN ./mvnw dependency:resolve
#
#COPY src ./src
#EXPOSE 8080
#CMD ["./mvnw", "spring-boot:run"]

#FROM eclipse-temurin:17-jdk-jammy
#WORKDIR /usr/src/app
#COPY target/vtc-stormbreaker-api-0.0.1-SNAPSHOT.jar app.jar
#EXPOSE 8080
#CMD ["java","-jar","app.jar"]
